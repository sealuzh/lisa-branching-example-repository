package org.ifi.seal.example;

public class ConcreteClass {

  private int instanceVariable;

  private int privateField = 1;
  protected int protectedField = 1;
  public int publicField = 1;

  public void method() { }

}
