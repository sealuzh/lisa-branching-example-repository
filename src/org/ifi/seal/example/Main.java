package org.ifi.seal.example;

public class Main {
  public static void main(String[] args) {

    ConcreteClass instance = new ConcreteClass();

    int x = 10;
    while (x > 0) {
      x--;
      --x;
    }
    assert(x <= 0);

    Demo d = new Demo(80);
    d.run();
    d.run();
  }
}

